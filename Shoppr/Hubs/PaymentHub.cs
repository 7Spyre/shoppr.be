﻿using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace Shoppr.Services
{
    public interface IPaymentHub
    {
        Task BroadcastResult(string transactionReference, string status);
    }

    public class PaymentTrackRequest
    {
        public string TransactionReference { get; set; }
        public string UserId { get; set; }
    }
    public class PaymentHub : Hub, IPaymentHub
    {
        protected IHubContext<PaymentHub> _context;
        public PaymentHub(IHubContext<PaymentHub> context)
        {
            _context = context;
        }
        public async override Task OnConnectedAsync()
        {
            await base.OnConnectedAsync();
        }

        public async Task BroadcastResult(string transactionReference, string status)
        {
            await _context.Clients.All.SendAsync(transactionReference, status);
        }
    }
}
