﻿using Microsoft.AspNetCore.SignalR;
using Newtonsoft.Json;
using Shoppr.Models.Entities;
using System.Threading.Tasks;

namespace Shoppr.Services
{
    public interface IOrderHub
    {
        Task BroadcastStatus(string orderId, Order order);
    }

    public class OrderTrackRequest
    {
        public string TransactionReference { get; set; }
        public string UserId { get; set; }
    }
    public class OrderHub : Hub, IOrderHub
    {
        protected IHubContext<OrderHub> _context;
        public OrderHub(IHubContext<OrderHub> context)
        {
            _context = context;
        }
        public async override Task OnConnectedAsync()
        {
            await base.OnConnectedAsync();
        }

        public async Task BroadcastStatus(string orderId, Order order)
        {
            var resp = JsonConvert.SerializeObject(order);
            await _context.Clients.All.SendAsync(orderId,  order);
        }
    }
}
