﻿using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using Shoppr.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shoppr.Repositories
{
	
	//Interface
	public interface IVoucherRepository {
		bool Connect();
		IEnumerable<Voucher> Get();
		Voucher GetById(string id);
		Voucher AddVoucher(Voucher store);
		Voucher UpdateStore(Voucher store);
	}


	//Impl
	public class VoucherRepository :IVoucherRepository
	{
		IMongoCollection<Voucher> _vouchers;

		private readonly IOptions<DatabaseSettings> _config;
		public VoucherRepository(IOptions<DatabaseSettings> config)
		{
			_config = config;
		}

		public bool Connect()
		{
			try{
				var t = _config.Value;
				var client = new MongoClient("mongodb+srv://admin_777:Juicey777&&&@cluster0.sn1mk.mongodb.net/shoppr?retryWrites=true&w=majority");
				var database = client.GetDatabase("shoppr");

				_vouchers = database.GetCollection<Voucher>("vouchers");
				return true;

			}
			catch(Exception e)
			{
				throw e;
			}
		}

		public IEnumerable<Voucher> Get()
		{
			Connect();
			return _vouchers.Find(x=>true).ToEnumerable();
		}

		public Voucher GetById(string id)
		{
			Connect();
			return _vouchers.Find(x=>x.id==id).ToEnumerable().FirstOrDefault();
		}

		public Voucher AddVoucher(Voucher voucher)
		{
			try
			{
				Connect();
				_vouchers.InsertOne(voucher);
				return voucher;
			}
			catch (Exception e)
			{
				throw e;
			}

		}
		public Voucher UpdateStore(Voucher voucher)
		{
			try
			{
				Connect();
				var filter = Builders<Voucher>.Filter.Eq("id", voucher.id);
				_vouchers.ReplaceOne(x => x.id == voucher.id, voucher);
				return voucher;
			}
			catch (Exception e)
			{
				throw e;
			}
		}
	}
}
