﻿using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using Shoppr.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shoppr.Repositories
{
	
	//Interface
	public interface IStoreRepository {
		bool Connect();
		IEnumerable<Store> Get();
		Store GetById(string id);
		Store AddStore(Store store);
		Store UpdateStore(Store store);
	}


	//Impl
	public class StoreRepository :IStoreRepository
	{
		IMongoCollection<Store> _stores;

		private readonly IOptions<DatabaseSettings> _config;
		public StoreRepository(IOptions<DatabaseSettings> config)
		{
			_config = config;
		}

		public bool Connect()
		{
			try{
				var t = _config.Value;
				var client = new MongoClient("mongodb+srv://admin_777:Juicey777&&&@cluster0.sn1mk.mongodb.net/shoppr?retryWrites=true&w=majority");
				var database = client.GetDatabase("shoppr");

				_stores = database.GetCollection<Store>("stores");
				return true;

			}
			catch(Exception e)
			{
				throw e;
			}
		}

		public IEnumerable<Store> Get()
		{
			Connect();
			return _stores.Find(x=>true).ToEnumerable();
		}

		public Store GetById(string id)
		{
			Connect();
			return _stores.Find(x=>x.id==id).ToEnumerable().FirstOrDefault();
		}

		public Store AddStore(Store store)
		{
			try
			{
				Connect();
				_stores.InsertOne(store);
				return store;
			}
			catch (Exception e)
			{
				throw e;
			}

		}
		public Store UpdateStore(Store store)
		{
			try
			{
				Connect();
				var filter = Builders<Store>.Filter.Eq("id", store.id);
				_stores.ReplaceOne(x => x.id == store.id, store);
				return store;
			}
			catch (Exception e)
			{
				throw e;
			}
		}
	}
}
