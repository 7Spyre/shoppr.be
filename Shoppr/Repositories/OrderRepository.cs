﻿using Microsoft.AspNetCore.Hosting.Server;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using Shoppr.Models.Entities;
using Shoppr.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Shoppr.Repositories
{
	//Interface
	public interface IOrderRepository {
		bool Connect();
		IEnumerable<Order> Get();
		Order GetOrder(string id);
		Order AddOrder(Order product);
		Order UpdateOrder(Order product);
	}


	//Impl
	public class OrderRepository :IOrderRepository
	{
		IMongoCollection<Order> _orders;
		private IMemoryCache _memoryCache;

		private readonly IOptions<DatabaseSettings> _config;
		public OrderRepository(IOptions<DatabaseSettings> config, IMemoryCache  memoryCache)
		{
			_config = config;
			_memoryCache = memoryCache;

		}

		public bool Connect()
		{
			try{
				var t = _config.Value;
				var client = new MongoClient("mongodb+srv://admin_777:Juicey777&&&@cluster0.sn1mk.mongodb.net/shoppr?retryWrites=true&w=majority");
				var database = client.GetDatabase("shoppr");

				_orders = database.GetCollection<Order>("orders");
				return true;

			}
			catch(Exception e)
			{
				throw e;
			}
		}

		public IEnumerable<Order> Get()
		{
			Connect();
			return _orders.Find(x=>true).ToEnumerable();
		}

		public Order GetOrder(string orderId)
		{
			var _order = _memoryCache.Get<Order>(orderId);
			if (_order == null)
			{
				_order = GetById(orderId);
				// add order to cache
				_memoryCache.Set(_order.id, _order);
			}

			return _order;
		}

		private Order GetById(string id)
		{
			Connect();
			return _orders.Find(x=>x.id==id).ToEnumerable()?.FirstOrDefault();
		}

		public Order AddOrder(Order order)
		{
			try
			{
				Connect();
				_orders.InsertOne(order);
				return order;
			}
			catch (Exception e)
			{
				throw e;
			}

		}

		public Order UpdateOrder(Order order)
		{
			try
			{
				Connect();
				var filter = Builders<Order>.Filter.Eq("id", order.id);
				order.dateLastUpdated = DateTime.Now;
				_orders.ReplaceOne(x => x.id == order.id, order);
				return order;
			}
			catch (Exception e)
			{
				throw e;
			}
		}

	}
}
