﻿using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Shoppr.Models;
using Shoppr.Models.Shared;

namespace Shoppr.Repositories
{
	//Model
	public class User
	{
		public User() { }
		public User (RegisterRequestModel model)
		{
			username = model.username;
			secret = model.secret;
			firstname = model.firstname;
			lastname = model.lastname;
			email = model.email;
			cellphone = model.cellphone;

		}


		[BsonId]
		[BsonRepresentation(BsonType.ObjectId)]
		public string id { get; set; }
		public string username { get; set; }
		public string secret { get; set; }
		public string firstname { get; set; }
		public string lastname { get; set; }
		public string email { get; set; }
		public string cellphone { get; set; }
		public string emailVerificationCode { get; set; }
		public string cellphoneVerificationCode { get; set; }
		public string pushNotificationToken { get; set; }
		public DateTime lastLogin { get; set; }
		public UserStatus status { get; set; }
		public GpsCoordinates lastKnownCoordinates { get; set; }

		public List<UserReview> userReviews { get; set; }
		public List<UserOrders> userOrders { get; set; }

	}



	//Interface
	public interface IUserRepository {
		IEnumerable<User> Get();
		User GetById(string id);
		User GetByEmail(string email);
		User VerifyLogin(string id, string password);
		User AddUser(User user);
		User UpdateUser(User user);
	}


	//Impl
	public class UserRepository :IUserRepository
	{
		IMongoCollection<User> _users;

		private readonly IOptions<DatabaseSettings> _config;
		public UserRepository(IOptions<DatabaseSettings> config)
		{
			_config = config;
		}

		private bool Connect()
		{
			try{
				var t = _config.Value;
				var client = new MongoClient(_config.Value.ConnectionString);
				var database = client.GetDatabase(_config.Value.DatabaseName);

				_users = database.GetCollection<User>(_config.Value.UserCollectionName);
				return true;

			}
			catch(Exception e)
			{
				throw e;
			}
		}

		public IEnumerable<User> Get()
		{
			Connect();
			return _users.Find(x=>true).ToEnumerable();
		}

		public User VerifyLogin(string id, string password)
		{
			try
			{
				var result = false;
				Connect();
				

				var user =  GetById(id);
				result = user?.secret == password;
				return result== true? user: null;
			}catch(Exception e)
			{
				throw e;
			}
		}

		public User AddUser(User user)
		{
			try
			{
				Connect();
				_users.InsertOne(user);
				return user;
			}
			catch (Exception e)
			{
				throw e;
			}
			
		}

		public User UpdateUser(User user)
		{
			try
			{
				Connect();
				var filter = Builders<User>.Filter.Eq("id", user.id);
				_users.ReplaceOne(x=>x.id ==user.id, user);
				return user;
			}
			catch (Exception e)
			{
				throw e;
			}
		}



		public User GetById(string id)
		{
			Connect();
			return _users.Find(x=>x.id==id)?.FirstOrDefault();
		}

		public User GetByEmail(string email)
		{
			Connect();
			return _users.Find(x => x.email == email)?.FirstOrDefault();
		}
	}

	//Classes

	public enum UserType
	{
		Facebook,
		Google,
		Email
	}

	public enum UserRole
	{
		Client,
		ServiceProvider,
		StoreOwner
	}


	public enum UserStatus
	{
		New,
		Verified,
		Active,
		Blocked
	}

	public class UserReview
	{
		public string id { get; set; }
		public string productId { get; set; }
		public string rating { get; set; }
		public DateTime dateCreated { get; set; }
	}

	public class UserOrders
	{
		public string id { get; set; }
		public string orderId { get; set; }
		public DateTime dateCreated { get; set; }

	}

}
