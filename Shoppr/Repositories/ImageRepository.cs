﻿using Microsoft.Extensions.Options;
using MongoDB.Driver;
using Shoppr.Models;
using Shoppr.Models.Entities;
using Shoppr.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Shoppr.Repositories
{
	//Interface
	public interface IImageRepository
	{
		bool Connect();
		IEnumerable<Image> Get();
		Image GetById(string id);
		Image AddImage(ImageRequestModel image);
		Image UpdateImage(ImageRequestModel image);
	}


	//Impl
	public class ImageRepository : IImageRepository
	{
		IMongoCollection<Image> _images;

		private readonly IOptions<DatabaseSettings> _config;
		private readonly IImageService _imageService;
		public ImageRepository(IOptions<DatabaseSettings> config, IImageService imageService)
		{
			_config = config;
			_imageService = imageService;
		}

		public bool Connect()
		{
			try{
				var t = _config.Value;
				var client = new MongoClient("mongodb+srv://admin_777:Juicey777&&&@cluster0.sn1mk.mongodb.net/shoppr?retryWrites=true&w=majority");
				var database = client.GetDatabase("shoppr");

				_images = database.GetCollection<Image>("images");
				return true;

			}
			catch(Exception e)
			{
				throw e;
			}
		}

		public IEnumerable<Image> Get()
		{
			Connect();
			return _images.Find(x=>true).ToEnumerable();
		}

		public Image GetById(string id)
		{
			Connect();
			return _images.Find(x=>x.id==id).ToEnumerable()?.FirstOrDefault();
		}

		public Image AddImage(ImageRequestModel imageRequest)
		{
			try
			{
				var result = _imageService.Upload(imageRequest);
				var image = new Image(imageRequest, result);
				Connect();
				_images.InsertOne(image);
				return image;
			}
			catch (Exception e)
			{
				throw e;
			}

		}

		public Image UpdateImage(ImageRequestModel image)
		{
			try
			{
				Connect();
				_imageService.Update(image);
				_images.ReplaceOne(x => x.publicId == image.publicId, image);
				return image;
			}
			catch (Exception e)
			{
				throw e;
			}
		}

	}
}
