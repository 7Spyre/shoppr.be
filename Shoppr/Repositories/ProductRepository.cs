﻿using Microsoft.AspNetCore.Hosting.Server;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using Shoppr.Models.Entities;
using Shoppr.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Shoppr.Repositories
{
	//Interface
	public interface IProductRepository {
		bool Connect();
		IEnumerable<Product> Get();
		Product GetById(string id);
		Product GetBySku(string sku);
		Product AddProduct(Product product);
		Product UpdateProduct(Product product);
		IEnumerable<Product> Search(string searchTerm);
	}


	//Impl
	public class ProductRepository :IProductRepository
	{
		IMongoCollection<Product> _products;
		ISession _session;

		private readonly IOptions<DatabaseSettings> _config;
		public ProductRepository(IOptions<DatabaseSettings> config, IImageService imageService)
		{
			_config = config;
		}

		public bool Connect()
		{
			try{
				var t = _config.Value;
				var client = new MongoClient("mongodb+srv://admin_777:Juicey777&&&@cluster0.sn1mk.mongodb.net/shoppr?retryWrites=true&w=majority");
				var database = client.GetDatabase("shoppr");

				_products = database.GetCollection<Product>("products");
				return true;

			}
			catch(Exception e)
			{
				throw e;
			}
		}

		public IEnumerable<Product> Get()
		{
			Connect();
			return _products.Find(x=>true).ToEnumerable();
		}

		public IEnumerable<Product> Search(string searchTerm)
		{
			Connect();
			//var match = allProducts.Where(x => x.title.ToLower().Contains(searchTerm.ToLower()) || x.description.ToLower().Contains(searchTerm.ToLower()));
			var result = _products.Find(Builders<Product>.Filter.Text(searchTerm)).ToList();

			return result;
		}

		public class ProductSearch
		{
			public string SearchTerm { get; set; }
		}


		public Product GetById(string id)
		{
			Connect();
			return _products.Find(x=>x.id==id).ToEnumerable()?.FirstOrDefault();
		}


		public Product GetBySku(string sku)
		{
			Connect();
			return _products.Find(x => x.barcode == sku).ToEnumerable()?.FirstOrDefault();
		}

		public Product AddProduct(Product product)
		{
			try
			{
				Connect();
				_products.InsertOne(product);
				return product;
			}
			catch (Exception e)
			{
				throw e;
			}

		}

		public Product UpdateProduct(Product product)
		{
			try
			{
				Connect();
				var filter = Builders<User>.Filter.Eq("id", product.id);
				_products.ReplaceOne(x => x.id == product.id, product);
				return product;
			}
			catch (Exception e)
			{
				throw e;
			}
		}

	}
}
