using System;
using System.IO;
using FirebaseAdmin;
using Google.Apis.Auth.OAuth2;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Shoppr.Repositories;
using Shoppr.Services;

namespace Shoppr
{
    public class Startup
	{
		public Startup(IConfiguration configuration)
		{
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		// This method gets called by the runtime. Use this method to add services to the container.
		public void ConfigureServices(IServiceCollection services)
		{
			services.AddCors();
			// Add functionality to inject IOptions<T>
			services.AddOptions();

			// Add our Config object so it can be injected
			services.Configure<DatabaseSettings>(Configuration.GetSection("DatabaseSettings"));

			services.AddControllers();

			services.AddSignalR();


			services.AddSwaggerGen();

			services.AddDistributedMemoryCache();
			services.AddSession(options => {
				options.IdleTimeout = TimeSpan.FromMinutes(1);//You can set Time   
			});

			services.AddMemoryCache();
			//DI
			services.AddTransient<IProductRepository, ProductRepository>();
			services.AddTransient<IUserRepository, UserRepository>();
			services.AddTransient<IImageService, ImageService>();
			services.AddTransient<IStoreRepository, StoreRepository>();
			services.AddTransient<IImageRepository, ImageRepository>();
			services.AddTransient<IGeoService, GeoService>();
			services.AddTransient<IPushNotificationService, PushNotificationService>();
			services.AddTransient<IOzowPaymentService, OzowPaymentService>();
			services.AddTransient<IOrderService, OrderService>();
			services.AddTransient<IOrderRepository, OrderRepository>();
			services.AddTransient<IOrderHub, OrderHub>();
			services.AddTransient<IPaymentHub, PaymentHub>();
			services.AddCors(options =>
			{
				options.AddPolicy("ClientPermission", policy =>
				{
					policy.AllowAnyHeader()
						.AllowAnyMethod()
						//.WithOrigins("http://localhost:3000")
						.AllowCredentials();
				});
			});

			var cred = GoogleCredential.FromJson(LoadGoogle());

			FirebaseApp.Create(new AppOptions() { Credential = cred });
			string LoadGoogle()
			{
				var contentRoot = Configuration.GetValue<string>(WebHostDefaults.ContentRootKey);

				using (StreamReader r = new StreamReader(contentRoot+"\\Resources\\qwikshop-4c809-firebase-adminsdk-pnekh-83e510500c.json"))
				{
					string json = r.ReadToEnd();
					return json;
				}
			}
		}
	

		// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
		public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
		{

			if (env.IsDevelopment())
			{
				app.UseDeveloperExceptionPage();
			}

			app.UseSwagger();
			app.UseSwaggerUI(c =>
			{
				c.SwaggerEndpoint("/swagger/v1/swagger.json", "Qwikshop");
			});
            app.UseCors(builder =>
            {

                builder
            .AllowAnyHeader()
                   .AllowAnyMethod()
                   .AllowAnyOrigin();
            });

            app.UseHttpsRedirection();

            //app.UseCors("ClientPermission");

            app.UseRouting();

			app.UseAuthorization();

			app.UseSession();

			app.UseEndpoints(endpoints =>
			{
				endpoints.MapControllers();
				endpoints.MapHub<PaymentHub>("/ws/payment/track");
				endpoints.MapHub<OrderHub>("/ws/order/track");
			});
		}
	}
}
