﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shoppr.Models
{
	public class RegisterRequestModel
	{
		public string username { get; set; }
		public string secret { get; set; }
		public string firstname { get; set; }
		public string lastname { get; set; }
		public string email { get; set; }
		public string cellphone { get; set; }
	}
}
