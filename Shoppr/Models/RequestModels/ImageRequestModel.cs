﻿using Shoppr.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shoppr.Models
{
	public class ProductRequestModel: Product
	{
		public bool isNew { get { return string.IsNullOrEmpty(id); }}
	
	}

	public class UploadProductRequestModel
    {
		public string sku { get; set; }
		public string image { get; set; }
    }

}
	