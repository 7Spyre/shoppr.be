﻿using Shoppr.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shoppr.Models
{
	public class StoreProximityRequestModel
	{
		public string latitude { get; set; }
		public string longitude { get; set; }
		public string searchTerm { get; set; }
		public string province { get; set; }
	
	}

}
