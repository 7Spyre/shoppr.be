﻿using Newtonsoft.Json;

namespace Shoppr.Models
{
	//public class InitializePaymentRequestModel
	//{
	//	[JsonProperty("authentication.userId")]
	//	public string authentication_userId { get; set; }

	//	[JsonProperty("authentication.password")]
	//	public string authentication_password { get; set; }

	//	[JsonProperty("authentication.entityId")]
	//	public string authentication_entityId { get; set; }

	//	[JsonProperty("merchantTransactionId")]
	//	public string merchantTransactionId { get; set; }

	//	[JsonProperty("amount")]
	//	public string amount { get; set; }

	//	[JsonProperty("paymentBrand")]
	//	public string paymentBrand { get; set; }

	//	[JsonProperty("paymentType")]
	//	public string paymentType { get; set; }

	//	[JsonProperty("currency")]
	//	public string currency { get; set; }

	//	[JsonProperty("shopperResultUrl")]
	//	public string shopperResultUrl { get; set; }

	//	[JsonProperty("merchantInvoiceId")]
	//	public string merchantInvoiceId { get; set; }
	//}



	public class InitiatePaymentRequestModel
	{
		[JsonProperty("SiteCode")]
		public string SiteCode { get; set; }

		[JsonProperty("CountryCode")]
		public string CountryCode { get; set; }

		[JsonProperty("CurrencyCode")]
		public string CurrencyCode { get; set; }

		[JsonProperty("Amount")]
		public string Amount { get; set; }

		[JsonProperty("TransactionReference")]
		public string TransactionReference { get; set; }

		[JsonProperty("BankReference")]
		public string BankReference { get; set; }

		[JsonProperty("Customer")]
		public string Customer { get; set; }

		[JsonProperty("CancelUrl")]
		public string CancelUrl { get; set; }

		[JsonProperty("ErrorUrl")]
		public string ErrorUrl { get; set; }

		[JsonProperty("SuccessUrl")]
		public string SuccessUrl { get; set; }
		[JsonProperty("NotifyUrl")]
		public string NotifyUrl { get; set; }

		[JsonProperty("IsTest")]
		public string IsTest { get; set; }

		[JsonProperty("BankId")]
		public string BankId { get; set; }

		[JsonProperty("BankAccountNumber")]
		public string BankAccountNumber { get; set; }

		[JsonProperty("BranchCode")]
		public string BranchCode { get; set; }

		[JsonProperty("BankAccountName")]
		public string BankAccountName { get; set; }

		[JsonProperty("PayeeDisplayName")]
		public string PayeeDisplayName { get; set; }

		[JsonProperty("ExpiryDateUtc")]
		public string ExpiryDateUtc { get; set; }

		[JsonProperty("AllowVariableAmount")]
		public string AllowVariableAmount { get; set; }


		[JsonProperty("Optional1")]
		public string Optional1 { get; set; }

		[JsonProperty("HashCheck")]
		public string HashCheck { get; set; }
	}
}
