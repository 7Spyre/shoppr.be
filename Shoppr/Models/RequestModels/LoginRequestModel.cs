﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shoppr.Models
{
	public class LoginRequestModel
	{
		public string id { get; set; }
		public string secret { get; set; }
	}

}
