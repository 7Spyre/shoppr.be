﻿using Shoppr.Models.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shoppr.Models
{
	public class AddProductToAisleRequest
	{
		public string storeId { get; set; }
		public string aisleId { get; set; }
		public StoreProduct product { get; set; }

	}
}
