﻿using Shoppr.Models.Entities;
using Shoppr.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shoppr.Models
{
	public class ProductResponseModel : Product
	{
		public ProductResponseModel(StoreProduct storeProduct, IProductRepository productRepository)
		{
			var product = productRepository.GetById(storeProduct.productId);

			id = product.id;
			title = product.title;
			barcode = product.barcode;
			weight = product.weight;
			description = product.description;
			category = product.category;
			size = product.size;
			dateCreated = DateTime.Now;
			isActive = product.isActive;
			creatingUser = product.creatingUser;
			brand = product.brand;
			tags = product.tags;
			price = storeProduct.price.ToString();
			salePrice = storeProduct.salePrice.ToString();
			state = storeProduct.state;

		}
		public string price { get; set; }
		public string salePrice { get; set; }
		public StoreProductState state { get; set; }

	
	}

}
