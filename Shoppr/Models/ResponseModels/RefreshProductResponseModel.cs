﻿using Shoppr.Models.Entities;
using Shoppr.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shoppr.Models
{
	public class RefreshProductResponseModel
	{
		public List<Product> products { get; set; }
		public List<string> tags { get; set; }
	}

}
