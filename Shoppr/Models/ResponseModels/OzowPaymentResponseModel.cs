﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shoppr.Models.ResponseModels
{

	public class OzowPaymentResponseModel
	{
		public string TransactionId { get; set; }
		public string TransactionReference { get; set; }
		public string Status { get; set; }
	}
}
