﻿using Newtonsoft.Json;
using Shoppr.Models.Entities;
using Shoppr.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shoppr.Models
{
	public class TrackPaymentResponseModel
	{
		[JsonProperty("TransactionId")]
		public string TransactionId { get; set; }

		[JsonProperty("MerchantCode")]
		public string MerchantCode { get; set; }

		[JsonProperty("SiteCode")]
		public string SiteCode { get; set; }

		[JsonProperty("TransactionReference")]
		public string TransactionReference { get; set; }

		[JsonProperty("CurrencyCode")]
		public string CurrencyCode { get; set; }

		[JsonProperty("Amount")]
		public string Amount { get; set; }

		[JsonProperty("Status")]
		public string Status { get; set; }

		[JsonProperty("StatusMessage")]
		public string StatusMessage { get; set; }

		[JsonProperty("CreatedDate")]
		public string CreatedDate { get; set; }

		[JsonProperty("PaymentDate")]
		public string PaymentDate { get; set; }

	}
	
}
