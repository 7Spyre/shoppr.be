﻿using Newtonsoft.Json;
using Shoppr.Models.Entities;
using Shoppr.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shoppr.Models
{
	public class InitializePaymentResponseModel
	{
		[JsonProperty("SiteCode")]
		public string SiteCode { get; set; }

		[JsonProperty("TransactionId")]
		public string TransactionId { get; set; }

		[JsonProperty("TransactionReference")]
		public string TransactionReference { get; set; }

		[JsonProperty("Amount")]
		public string Amount { get; set; }

		[JsonProperty("Status")]
		public string Status { get; set; }

		[JsonProperty("Optional1")]
		public string Optional1 { get; set; }

		[JsonProperty("CurrencyCode")]
		public string CurrencyCode { get; set; }

		[JsonProperty("IsTest")]
		public string IsTest { get; set; }

		[JsonProperty("StatusMessage")]
		public string StatusMessage { get; set; }

		[JsonProperty("Hash")]
		public string Hash { get; set; }

	

	}
	
}
