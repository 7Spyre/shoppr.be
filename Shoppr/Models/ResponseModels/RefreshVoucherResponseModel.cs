﻿using Shoppr.Models.Entities;
using Shoppr.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shoppr.Models
{
	public class RefreshVoucherResponseModel
	{
		public List<Voucher> vouchers { get; set; }
	}

}
