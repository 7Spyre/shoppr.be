﻿using Shoppr.Models.Entities;
using Shoppr.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shoppr.Models
{
	public class StoreResponseModel : Store
	{
		public StoreResponseModel(Store store, IProductRepository productRepo)
		{
			allproducts = new List<Product>();
			store.aisles.ForEach(c => c.products.ForEach(c=>GetProducts(productRepo.GetById(c.productId))));
			void GetProducts(Product product)
			{
				if (product != null)
				{
					allproducts.Add(product);
				}
			
			}
			id = store.id;
			name = store.name;
			latitude = store.latitude;
			longitude = store.longitude;
			province = store.province;
			suburb = store.suburb;
			aisles = store.aisles;
			brand = store.brand;
			contact = store.contact;
			address = store.address;

		}
		public List<Product> allproducts { get; set; }
	
	}

}
