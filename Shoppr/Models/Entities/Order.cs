﻿using CloudinaryDotNet.Actions;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Shoppr.Models.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shoppr.Models.Entities
{
	public class Order
	{
		public Order() { }
		public Order(OrderRequestModel request) {

			id = request.id;
			status = "Pending";
			customer = request.customer;
			deliveryPartner = request.deliveryPartner;
			dateCreated = DateTime.Now;
			dateLastUpdated = DateTime.Now;
			products = request.products;
			deliveryAddress = request.deliveryAddress;
			storeId = request.storeId;
			orderTotal = request.orderTotal;
			totalVat = request.totalVat;
			paymentReference = "";
		}
		
		[BsonId]
		[BsonRepresentation(BsonType.ObjectId)]
		public string id { get; set; }
		public string status { get; set; }
		public string customer { get; set; }
		public string deliveryPartner { get; set; }
		public DateTime dateCreated { get; set; }
		public DateTime dateLastUpdated { get; set; }
		public List<StoreProductOrder> products { get; set; }
		public List<TrackOrderProductResponseModel> productsInCart { get; set; }
		public List<TrackOrderProductResponseModel> remainingProducts { get; set; }
		public DeliveryAddress deliveryAddress { get; set; }
		public string storeId { get; set; }
		public string orderTotal { get; set; }
		public string totalVat { get; set; }
		public string paymentReference { get; set; }

	}

	public class StoreProductOrder
	{
	
		public string sku { get; set; }
		public int quantity { get; set; }
	}

	public class StoreProductOrderUpdateRequest : StoreProductOrder
	{
		public string id { get; set; }
		public string orderId { get; set; } 
    }

	public class TrackOrderProductResponseModel : StoreProductOrderUpdateRequest
    {
		public string productName { get; set; }
		public string productImage { get; set; }

    }
	public class DeliveryAddress
	{
		public GpsCoordinates coordinates { get; set; }
		public string street { get; set; }
		public string unitNumber { get; set; }
		public string complex { get; set; }
		public string suburb { get; set; }
	}

	public enum OrderStatus
	{
		Pending,
		Paid,
		Assigned,
		AwaitingDelivery,
		OngoingDelivery,
		Delivered,
		Complete,
		Cancelled,
		Refunded
	}

}
