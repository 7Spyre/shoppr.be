﻿using CloudinaryDotNet.Actions;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shoppr.Models.Entities
{
	public class Product
	{
		public Product() { }
		public Product(ProductRequestModel request)
		{
			id = request.id;
			title = request.title;
			barcode = request.barcode;
			weight = request.weight;
			description = request.description;
			category = request.category;
			size = request.size;
			dateCreated = DateTime.Now;
			isActive = request.isActive;
			creatingUser = request.creatingUser;
			brand = request.brand;
			tags = request.tags;
		}

		[BsonId]
		[BsonRepresentation(BsonType.ObjectId)]
		public string id { get; set; }
		public string title { get; set; }
		public List<ProductImage> images { get; set; }
		public string barcode { get; set; }
		public string weight { get; set; }
		public string description { get; set; }
		public ProductCategory category { get; set; }
		public ProductSize size { get; set; }
		public DateTime dateCreated { get; set; }
		public bool isActive { get; set; }
		public string creatingUser { get; set; }
		public string brand { get; set; }
		public string tags { get; set; }


	}

	public class ProductImage
	{
		public ProductImage(Image image)
		{
			publicUrl = image.url;
			id = image.id;
		}
		public ProductImage() { }
		public string publicUrl { get; set; }
		public string id { get; set; }
	}


	public enum ProductCategory
	{
		All=0,
		Food=1,
		Beverages=2,
		Toiletries=3,
		Household=4
	}
	public enum ProductSize
	{
		All=0,
		XXS=1,
		XS=2,
		S=3,
		M=4,
		L=5,
		XL=6,
		XXL=7
	}
}
