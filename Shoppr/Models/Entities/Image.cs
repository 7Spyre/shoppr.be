﻿using CloudinaryDotNet.Actions;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shoppr.Models.Entities
{
	public class Image
	{
		public Image()
		{

		}
		public Image(ImageRequestModel requestModel, ImageUploadResult result)
		{
			id = requestModel.id;
			userid = requestModel.userid;
			type = requestModel.type;
			publicId = result.PublicId;
			refId = requestModel.refId;
			url = result.SecureUrl.ToString();
		}

		public string id { get; set; }
		public string data { get; set; }
		public string userid { get; set; }
		public ImageType type { get; set; }
		public string publicId { get; set; }
		public string refId { get; set; }
		public string url { get; set; }
	}
	public enum ImageType
	{
		Product,
		Store,
		User,

	}

}
