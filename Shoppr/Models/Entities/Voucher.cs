﻿using CloudinaryDotNet.Actions;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shoppr.Models.Entities
{
	public class Voucher
	{
		public Voucher() { }
		
		[BsonId]
		[BsonRepresentation(BsonType.ObjectId)]
		public string id { get; set; }
		public string hash { get; set; }
		public DateTime createdDate { get; set; }
		public double amount { get; set; }
		public DateTime lastUsed { get; set; }
		public string merchant { get; set; }
		public VoucherStatus status { get; set; }
	}

	public enum VoucherStatus
	{
		Available,
		InUse,
		Depleted
	}

}
