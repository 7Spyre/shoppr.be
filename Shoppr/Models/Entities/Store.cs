﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shoppr.Models.Entities
{

	public class Store
	{
		public Store()
		{

		}
		public Store(StoreRequestModel store)
		{
			id = store.id;
			name = store.name;
			latitude = store.latitude;
			longitude = store.longitude;
			province = store.province;
			suburb = store.suburb;
			aisles = store.aisles?? new List<Aisle>();
			brand = store.brand;
			contact = store.contact;
			address = store.address;
		}

		[BsonId]
		[BsonRepresentation(BsonType.ObjectId)]
		public string id { get; set; }
		public string name { get; set; }
		public string latitude { get; set; }
		public string longitude { get; set; }
		public string province { get; set; }
		public string suburb { get; set; }
		public List<Aisle> aisles { get; set; }
		public string storeImage { get; set; } = "https://res.cloudinary.com/do5ln1v1h/image/upload/v1611179883/storefront_xvrysj.jpg";
		public string address { get; set; }
		public string brand { get; set; }
		public string contact { get; set; }
	}

	public class Aisle
	{
		public List<StoreProduct> products { get; set; }
		public string title { get; set; }
		public string aisleId { get; set; }
	}

	public class StoreProduct
	{
		public StoreProduct() { }
		public StoreProduct(Product product,double _price, double _salePrice=0, StoreProductState _state = StoreProductState.InStock, int _index=0, bool _inStock = true)
		{
			productId = product.id;
			index = _index;
			price = _price.ToString();
			salePrice = _salePrice.ToString();
			state = _state;
		}
		public string productId { get; set; }
		public int index { get; set; }
		public string price { get; set; }
		public string salePrice { get; set; }
		public StoreProductState state { get; set; }
	}

	public enum StoreProductState
	{
		InStock = 0,
		OutOfStock = 1,
		Special = 2,
	}

}
