﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shoppr.Models.Shared
{

	public class GpsCoordinates
	{
		public string longitude { get; set; }
		public string latitude { get; set; }

	}
}
