﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shoppr.Models.Enums
{
	public enum SortTypeEnum
	{
		ALPH_ASC,
		ALPH_DESC,
		PRICE_ASC,
		PRICE_DESC,
		POPULAR_DESC,
		POPULAR_ASC
		
	}
}
