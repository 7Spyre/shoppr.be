﻿using FirebaseAdmin.Messaging;
using Shoppr.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace Shoppr.Services
{
	public interface IPushNotificationService
	{
		public Task<bool> SendDeliveryAlert(List<User> user);
		public Task<bool> SendDeliveryAlert(string token);
	}
	public class PushNotificationService: IPushNotificationService
	{
		public async Task<bool> SendDeliveryAlert(List<User> users)
		{

			// Create a list containing up to 500 registration tokens.
			// These registration tokens come from the client FCM SDKs.
			var registrationTokens = new List<string>();
			users.ForEach(x => registrationTokens.Add(x.pushNotificationToken));
			
			var message = new MulticastMessage()
			{
				Tokens = registrationTokens,
				Data = new Dictionary<string, string>()
				{
					{ "score", "850" },
					{ "time", "2:45" },
				},
				Android= new AndroidConfig()
				{
					Notification = new AndroidNotification()
					{
						Title="New delivery in your area",
						Body="Body",
						Sound="delivery.mp3"
					}
				}
			};

			var response = await FirebaseMessaging.DefaultInstance.SendMulticastAsync(message);
			// See the BatchResponse reference documentation
			// for the contents of response.
			//Console.WriteLine($"{response.SuccessCount} messages were sent successfully");


			return true;
		}

		public async Task<bool> SendDeliveryAlert(string token)
		{

			// Create a list containing up to 500 registration tokens.
			// These registration tokens come from the client FCM SDKs.
			var registrationTokens = new List<string>();
			registrationTokens.Add("dZ743oOLTzGz-tTQyvqcbi:APA91bHwiuOKSH11Q9NZVBKGsEeKmdugvd4OBPH4KXi2RTl5-SRDt2Vh2hlb3H7PZnDtAr1HrYRaZ0G6Fu-dXN3CyAK5j2NTYFP7LmeyIjzYA2Nd8LFG7UVXGEkEibXULzqIjFn9uthJ");

			var message = new MulticastMessage()
			{
				Tokens = registrationTokens,
				Data = new Dictionary<string, string>()
				{
					{ "score", "850" },
					{ "time", "2:45" },
				},
				Android = new AndroidConfig()
				{
					Notification = new AndroidNotification()
					{
						Title = "New delivery in your area",
						Body = "Body",
						Sound = "delivery.mp3"
					}
				}
			};

			var response = await FirebaseMessaging.DefaultInstance.SendMulticastAsync(message);
			// See the BatchResponse reference documentation
			// for the contents of response.
			//Console.WriteLine($"{response.SuccessCount} messages were sent successfully");


			return true;
		}
		
	}



}
