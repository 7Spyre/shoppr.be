﻿using System;
using System.Threading.Tasks;
using System.Text;
using System.Security.Cryptography;
using Shoppr.Models;
using Newtonsoft.Json;
using System.Net.Http;
using Microsoft.Extensions.Options;

namespace Shoppr.Services
{
	public interface IOzowPaymentService
	{
		public Task<InitializePaymentResponseModel> InitializePayment(InitiatePaymentRequestModel requestModel);
		public Task<TrackPaymentResponseModel> TrackPayment(string transactionId);
		string HashPayload(string payload);
		Task<string> GeneratePayloadHash();
	}
	public class OzowPaymentService: IOzowPaymentService
	{
		private readonly IOptions<OzowSettings> _config;
		private readonly IPaymentHub _paymentHub;

		public OzowPaymentService(IOptions<OzowSettings> config, IPaymentHub paymentHub)
		{
			_config = config;
			_paymentHub = paymentHub;
		}

		public async Task<InitializePaymentResponseModel> InitializePayment(InitiatePaymentRequestModel request)
		{
			request.SiteCode = _config.Value.SiteCode;
			request.CountryCode = _config.Value.CountryCode; 
			request.CurrencyCode = _config.Value.CurrencyCode; 
			request.SuccessUrl = _config.Value.SuccessUrl; 
			request.ErrorUrl = _config.Value.ErrorUrl; 
			request.CancelUrl = _config.Value.CancelUrl; 
			request.NotifyUrl = _config.Value.NotifyUrl;

			request.SiteCode = "TSTSTE0001";
			request.CountryCode = "ZA";
			request.CurrencyCode = "ZAR";
			request.SuccessUrl ="http://test.i-pay.co.za/responsetest.php";
			request.ErrorUrl = "http://test.i-pay.co.za/responsetest.php";
			request.CancelUrl = "http://test.i-pay.co.za/responsetest.php";
			request.NotifyUrl = "http://test.i-pay.co.za/responsetest.php";
			request.CountryCode = "ZA";
			request.CurrencyCode = "ZAR";
			request.Amount = "0.01";
			request.IsTest = "true";
			request.BankReference = "UniqueRef-20-Or-Less";
			
			//var siteCode = "TSTSTE0001";
			//var privateKey = "215114531AFF7134A94C88CEEA48E";
			//var countryCode = "ZA";
			//var currencyCode = "ZAR";
			//var amount = "0.01";
			//var transRef = "Merchant-Unique-Ref";
			//var bankRef = "UniqueRef-20-Or-Less";
			//var cancelUrl = "http://test.i-pay.co.za/responsetest.php";
			//var errorUrl = "http://test.i-pay.co.za/responsetest.php";
			//var successUrl = "http://test.i-pay.co.za/responsetest.php";
			//var notifyUrl = "http://test.i-pay.co.za/responsetest.php";
			//var isTest = "false";



			var payloadHash = await Task.Run(() => GeneratePayloadHash());
			request.HashCheck = payloadHash;
			var payload = JsonConvert.SerializeObject(request);

			var httpContent = new StringContent(payload, Encoding.UTF8, "application/json");

			using (var httpClient = new HttpClient())
			{
				var httpResponse = await httpClient.PostAsync("https://pay.ozow.com/", httpContent);

				if (httpResponse.Content != null)
				{
					var responseContent = await httpResponse.Content.ReadAsStringAsync();
					var response = JsonConvert.DeserializeObject<InitializePaymentResponseModel>(responseContent);
					//Add to cache

					return response;
				}
				throw new Exception("Bad Response");
			}

		}

		public async Task<TrackPaymentResponseModel> TrackPayment(string transactionId)
		{
			var siteCode = _config.Value.SiteCode;
			//check cache

			using (var httpClient = new HttpClient())
			{
				var httpResponse = await httpClient.GetAsync(GenerateUrl(siteCode, transactionId));

				if (httpResponse.Content != null)
				{
					var responseContent = await httpResponse.Content.ReadAsStringAsync();
					var response = JsonConvert.DeserializeObject<TrackPaymentResponseModel>(responseContent);
 
					//broadcast message
					await _paymentHub.BroadcastResult(transactionId, response.Status);
					return response;
				}
				throw new Exception("Bad Response");
			}

			string GenerateUrl(string siteCode, string transactionId)
			{
				var result = $"https://api.ozow.com/GetTransaction?siteCode={siteCode}&transactionId={transactionId}";
				return result;
			}

		}

		public bool VoidPayment(string transactionId)
		{
			return true;
		}

		//private async Task<string> GeneratePayloadHash(InitiatePaymentRequestModel request)
			public async Task<string> GeneratePayloadHash()
		{
			//TEST
			var siteCode = "TSTSTE0001";
			var privateKey = "215114531AFF7134A94C88CEEA48E";
			var countryCode = "ZA";
			var currencyCode = "ZAR";
			var amount = "0.01";
			var transRef = "Merchant-Unique-Ref";
			var bankRef = "UniqueRef-20-Or-Less";
			var cancelUrl = "http://test.i-pay.co.za/responsetest.php";
			var errorUrl = "http://test.i-pay.co.za/responsetest.php";
			var successUrl = "http://test.i-pay.co.za/responsetest.php";
			var notifyUrl = "http://test.i-pay.co.za/responsetest.php";
			var isTest = "false";


			//
			//var siteCode = _config.Value.SiteCode;
			//var countryCode = _config.Value.CountryCode;
			//var currencyCode = _config.Value.CurrencyCode;
			//var errorUrl = _config.Value.ErrorUrl;
			//var successUrl = _config.Value.SuccessUrl;
			//var cancelUrl = _config.Value.CancelUrl;
			//var notifyUrl = _config.Value.NotifyUrl;
			//var privateKey = _config.Value.PrivateKey;

			//var payload = $"{siteCode}{countryCode}{currencyCode}{request.Amount}{request.TransactionReference}{request.BankReference}{cancelUrl}{errorUrl}{notifyUrl}{request.IsTest}{privateKey}";
			var payload = $"{siteCode}{countryCode}{currencyCode}{amount}{transRef}{bankRef}{cancelUrl}{errorUrl}{successUrl}{notifyUrl}{isTest}{privateKey}";
			var lcPayload = payload.ToLower();
			var result = HashPayload(lcPayload);
			return result;
		}

		public string HashPayload(string payload)
		{
			//payload = "tstste0001zazar25.00123abc123http://demo.ozow.com/cancel.aspxhttp://demo.ozow.com/cancel.aspxhttp://demo.ozow.com/success.aspxhttp://demo.ozow.com/notify.aspxfalse215114531aff7134a94c88ceea48e";
			//payload = "TSTSTE0001ZAZAR0.011234512345http://test.i-pay.co.za/responsetest.phphttp://test.i-pay.co.za/responsetest.phphttp://test.i-pay.co.za/responsetest.phphttp://test.i-pay.co.za/responsetest.phpfalse215114531aff7134a94c88ceea48e";
			//payload = "tstste0001zazar0.01test1test112345testcustomerhttp://test.i-pay.co.za/responsetest.phphttp://test.i-pay.co.za/responsetest.phphttp://test.i-pay.co.za/responsetest.phphttp://test.i-pay.co.za/responsetest.phpfalse215114531aff7134a94c88ceea48e";
			//payload = "tstste0001zazar0.01merchant-unique-refuniqueref-20-or-lesshttp://test.i-pay.co.za/responsetest.phphttp://test.i-pay.co.za/responsetest.phphttp://test.i-pay.co.za/responsetest.phphttp://test.i-pay.co.za/responsetest.phpfalse215114531aff7134a94c88ceea48e";


			string result;
			HashHMAC(payload);

			void HashHMAC(string message)
			{
				using (SHA512 sha512Hash = SHA512.Create())
				{
					//From String to byte array
					byte[] sourceBytes = Encoding.UTF8.GetBytes(message);
					byte[] hashBytes = sha512Hash.ComputeHash(sourceBytes);
					result = BitConverter.ToString(hashBytes).Replace("-", String.Empty);

				}
			}
			return result;
		}

	}
}
