﻿using System;
using System.Threading.Tasks;
using Shoppr.Models;
using Shoppr.Models.Entities;
using Shoppr.Repositories;
using Microsoft.Extensions.Caching.Memory;
using System.Collections.Generic;
using System.Linq;

namespace Shoppr.Services
{
    public interface IVoucherService
	{

		public Task<Order> CreateOrder(OrderRequestModel requestModel);
		public Task<List<Order>> GetPendingOrders();
		public Task<List<bool>> SendOrderToDrivers(Order order);
		//public Order AddPurchaseToCart(string orderId, string sku);
		public Order InitializeOrderTracking(string orderId);
	}
	public class VoucherService : IVoucherService
	{
		private IOrderRepository _orderRepository;
		private IMemoryCache _memoryCache;
		private IGeoService _geoService;
		private IOrderHub _orderHub;
		private IPushNotificationService _notificationService;
		public VoucherService(IOrderRepository orderRepository, IMemoryCache memoryCache, IGeoService geoService, IPushNotificationService pushNotificationService,IOrderHub orderHub)
		{
			_orderRepository = orderRepository;
			_memoryCache = memoryCache;
			_geoService = geoService;
			_notificationService = pushNotificationService;
			_orderHub = orderHub;
		}

		public async Task<Order> CreateOrder(OrderRequestModel requestModel)
		{
			var order = new Order(requestModel);
			_orderRepository.AddOrder(order);

			return order;
		}

		public async Task<List<Order>> GetPendingOrders()
		{
			var currentOrders = CacheExtensions.Get<List<Order>>(_memoryCache, "CURRENT_ORDERS") ?? new List<Order>();
			return currentOrders;
		}


		public async Task<List<bool>> SendOrderToDrivers(Order order)
		{
			var responses = new List<bool>();
			var activeDrivers = CacheExtensions.Get<List<User>>(_memoryCache, "ACTIVE_DRIVERS") ?? new List<User>();
			var activeDriversRanges = activeDrivers.Select(x => new Tuple<string, double>(x.pushNotificationToken, _geoService.CalculateDistance(new Location(order.deliveryAddress.coordinates), new Location(x.lastKnownCoordinates))));
			var closestDrivers = activeDriversRanges.OrderBy(x => x.Item2);
			if (closestDrivers.FirstOrDefault().Item2 > 5)
			{
				//no driver within 5km
			}
			else
			{
				var driversInRange = closestDrivers.Where(x => x.Item2 <= 5);

				
				foreach(var driver in driversInRange)
				{
					var response = await _notificationService.SendDeliveryAlert(driver.Item1);

					responses.Add(response);
				}				
			}



			return responses;

		}

		public Order InitializeOrderTracking (string orderId)
        {
			var _order = _orderRepository.GetOrder(orderId);
			_orderHub.BroadcastStatus(_order.id,_order);

			return _order;

        }
		//public Order AddPurchaseToCart(string orderId, string sku)
		//{
		//	var _order = _orderRepository.GetOrder(orderId);
		//	var product = _order.products.First(x => x.sku == sku);

		//	_order.productsInCart.Add(product);
		//	_orderHub.BroadcastStatus(_order.id, _order);

		//	return _order;

		//}



	}



}
