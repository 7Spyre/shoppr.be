﻿using CloudinaryDotNet;
using CloudinaryDotNet.Actions;
using Microsoft.Extensions.Options;
using Shoppr.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace Shoppr.Services
{
	public interface IImageService
	{
		public ImageUploadResult Upload(ImageRequestModel image);
		public ImageUploadResult Update(ImageRequestModel image);
	}

	
	public class ImageService:IImageService
	{
		private readonly IOptions<CloudinarySettings> _config;
		public ImageUploadResult Upload(ImageRequestModel image)
		{
			var cloudinary = new Cloudinary(new Account() { ApiKey = "331993555164334", ApiSecret = "Ao4Bk7Lk61cHUvpdw562Al5mRVY", Cloud = "do5ln1v1h" });
			var uploadParams = new ImageUploadParams()
			{
				File = new FileDescription(@"data:image/png;base64,"+image.data),
			//	File = new FileDescription(@"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAUAAAAFCAYAAACNbyblAAAAHElEQVQI12P4//8/w38GIAXDIBKE0DHxgljNBAAO9TXL0Y4OHwAAAABJRU5ErkJggg==")
			};
			var uploadResult = cloudinary.Upload(uploadParams);

			return uploadResult;
		}
		public ImageUploadResult Update(ImageRequestModel image)
		{
			var cloudinary = new Cloudinary(new Account() { ApiKey = "331993555164334", ApiSecret = "Ao4Bk7Lk61cHUvpdw562Al5mRVY", Cloud = "do5ln1v1h" });
			
			var uploadParams = new ImageUploadParams()
			{
				PublicId = image.publicId,
				File = new FileDescription(image.data)
			};
			var uploadResult = cloudinary.Upload(uploadParams);
			return uploadResult;
		}

	}
}
