﻿using System;
using System.Threading.Tasks;
using Shoppr.Models;
using Shoppr.Models.Entities;
using Shoppr.Repositories;
using Microsoft.Extensions.Caching.Memory;
using System.Collections.Generic;
using System.Linq;

namespace Shoppr.Services
{
    public interface IOrderService
	{

		public Task<Order> CreateOrder(OrderRequestModel requestModel);
		public Task<List<Order>> GetPendingOrders();
		public Task<List<bool>> SendOrderToDrivers(Order order);
		public Order AddPurchaseToCart(string orderId, string sku);
		public Order InitializeOrderTracking(string orderId);
	}
	public class OrderService: IOrderService
	{
		private IOrderRepository _orderRepository;
		private IProductRepository _productRepository;
		private IMemoryCache _memoryCache;
		private IGeoService _geoService;
		private IOrderHub _orderHub;
		private IPushNotificationService _notificationService;
		public OrderService(IOrderRepository orderRepository, IMemoryCache memoryCache, IGeoService geoService, IPushNotificationService pushNotificationService,IOrderHub orderHub, IProductRepository productRepository)
		{
			_orderRepository = orderRepository;
			_memoryCache = memoryCache;
			_geoService = geoService;
			_notificationService = pushNotificationService;
			_orderHub = orderHub;
			_productRepository = productRepository;
		}

		public async Task<Order> CreateOrder(OrderRequestModel requestModel)
		{
			var order = new Order(requestModel);
			_orderRepository.AddOrder(order);

			return order;
		}

		public async Task<List<Order>> GetPendingOrders()
		{
			var currentOrders = CacheExtensions.Get<List<Order>>(_memoryCache, "CURRENT_ORDERS") ?? new List<Order>();
			return currentOrders;
		}


		public async Task<List<bool>> SendOrderToDrivers(Order order)
		{
			var responses = new List<bool>();
			var activeDrivers = CacheExtensions.Get<List<User>>(_memoryCache, "ACTIVE_DRIVERS") ?? new List<User>();
			var activeDriversRanges = activeDrivers.Select(x => new Tuple<string, double>(x.pushNotificationToken, _geoService.CalculateDistance(new Location(order.deliveryAddress.coordinates), new Location(x.lastKnownCoordinates))));
			var closestDrivers = activeDriversRanges.OrderBy(x => x.Item2);
			if (closestDrivers.FirstOrDefault().Item2 > 5)
			{
				//no driver within 5km
			}
			else
			{
				var driversInRange = closestDrivers.Where(x => x.Item2 <= 5);

				
				foreach(var driver in driversInRange)
				{
					var response = await _notificationService.SendDeliveryAlert(driver.Item1);

					responses.Add(response);
				}				
			}



			return responses;

		}

		public Order InitializeOrderTracking (string orderId)
        {
			var _order = _orderRepository.GetOrder(orderId);
			_orderHub.BroadcastStatus(_order.id,_order);

			return _order;

        }
		public Order AddPurchaseToCart(string orderId, string sku)
		{
			var _order = _orderRepository.GetOrder(orderId);

		
			var targetProduct = _order.remainingProducts.FirstOrDefault(x => x.sku == sku);
			targetProduct.quantity = targetProduct.quantity > 0 ? targetProduct.quantity - 1 : 0;

			_orderRepository.UpdateOrder(_order);
			_orderHub.BroadcastStatus(orderId, _order);

			return _order;

		}



	}



}
