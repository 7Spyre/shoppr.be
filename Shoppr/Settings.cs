using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace Shoppr
{
	public class Settings { 
	
		public CloudinarySettings CloudinarySettings { get; set; }
		public DatabaseSettings DatabaseSettings { get; set; }
		public OzowSettings OzowSettings { get; set; }
	}

	public class DatabaseSettings
	{
		public string UserCollectionName { get; set; }
		public string ProductCollectionName { get; set; }
		public string ConnectionString { get; set; }
		public string DatabaseName { get; set; }
		public string SuccessUrl { get; set; }
		public string ErrorUrl { get; set; }


	}
	public class CloudinarySettings
	{
		public string ApiKey { get; set; }
		public string ApiSecret { get; set; }
		public string CloudUrl { get; set; }
		public string CloudName { get; set; }	
	}
	public class OzowSettings
	{
		public string SiteCode { get => "TSTSTE0001"; }
		public string APIKey { get => "EB5758F2C3B4DF3FF4F2669D5FF5B"; }
		public string PrivateKey { get => "215114531AFF7134A94C88CEEA48E"; }
		public string CountryCode { get => "ZA"; }
		public string CurrencyCode { get => "ZAR"; }
		public string SuccessUrl { get => "http://www.google.com/success"; }
		public string ErrorUrl { get => "http://www.google.com/error"; }
		public string CancelUrl { get => "http://www.google.com/cancel"; }
		public string NotifyUrl { get => "http://www.google.com/notify"; }
	}

}