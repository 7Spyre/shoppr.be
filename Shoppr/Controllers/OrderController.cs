﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Shoppr.Models;
using Shoppr.Models.Entities;
using Shoppr.Repositories;
using Microsoft.Extensions.Caching.Memory;
using Shoppr.Services;
using System.Collections.Generic;

namespace Shoppr.Controllers
{
    [ApiController]
	[Route("/orders")]
	public class OrderController : BaseController
	{
		private readonly IOptions<DatabaseSettings> _config;
		private IOrderRepository _orderRepository;
		private IStoreRepository _storeRepository;
		private IImageRepository _imageRepository;
		private IOrderService _orderService;
		private IMemoryCache _memoryCache;
		private IProductRepository _productRepository;
		private readonly ILogger<OrderController> _logger;


		public OrderController(ILogger<OrderController> logger, IOrderRepository orderRepository, IImageRepository imageRepository, IMemoryCache memoryCache, IStoreRepository storeRepository, IOrderService orderService, IProductRepository productRepository)
		{
			_logger = logger;
			_orderRepository = orderRepository;
			_imageRepository = imageRepository;
			_memoryCache = memoryCache;
			_storeRepository = storeRepository;
			_orderService = orderService;
			_productRepository = productRepository;
		}


		[HttpGet("/orders")]
		public ActionResult GetAll()
		{
			var products = _orderRepository.Get();
			return new JsonResult(products);
		}

		[HttpGet("/orders/{id}")]
		public ActionResult Get(string id)
		{
			var order = _orderRepository.GetOrder(id);

			if (order.productsInCart == null) {
				order.productsInCart = new List<TrackOrderProductResponseModel>();
				foreach(var prod in order.products)
                {
					var productData = _productRepository.GetById(prod.sku);
					order.productsInCart.Add(new TrackOrderProductResponseModel() { id = productData.id, orderId = productData.id, productImage = productData.images?[0].publicUrl, productName = productData.title, quantity = prod.quantity, sku = productData.barcode });
				}
				order.remainingProducts = order.productsInCart;
				_orderRepository.UpdateOrder(order);
			}
			return new JsonResult(order);
		}

	
		[HttpPost("/orders/create")]
		public ActionResult<User> Create([FromBody] OrderRequestModel request)
		{
			request.id = null;
			var newOrder = new Order(request);
			var response = _orderRepository.AddOrder(newOrder);
			_orderService.InitializeOrderTracking(response.id);
			return new JsonResult(response);
		}

		

		[HttpPost("/orders/update")]
		public ActionResult<User> Update([FromBody] OrderRequestModel request)
		{
			var order = new Order(request);
			var response = _orderRepository.UpdateOrder(order);
			return new JsonResult(response);
		}

		[HttpPost("/orders/updateOrderItem/")]
		public ActionResult<Order> UpdateOrderItem([FromBody] StoreProductOrderUpdateRequest request)
		{
			var response = _orderService.AddPurchaseToCart(request.orderId, request.sku);
			return new JsonResult(response.productsInCart);
		}

		[HttpGet("/orders/getOrderItems")]
		public ActionResult<Order> GetOrderItems(string orderId)
		{
			var response = _orderRepository.GetOrder(orderId);
			return new JsonResult(response.productsInCart);
		}




	}
}
