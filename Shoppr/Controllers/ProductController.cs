﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Shoppr.Models;
using Shoppr.Models.Entities;
using Shoppr.Repositories;
using Microsoft.AspNetCore.Http;
using Shoppr.Extensions;
using System.Linq;
using static Shoppr.Models.Entities.Product;
using Microsoft.Extensions.Caching.Memory;
using System.Collections.Generic;
using Shoppr.Models.Enums;
using System;
using Shoppr.Services;

namespace Shoppr.Controllers
{
	[ApiController]
	[Route("/product")]
	public class ProductController : BaseController
	{
		private readonly IOptions<DatabaseSettings> _config;
		private IProductRepository _productRepository;
		private IStoreRepository _storeRepository;
		private IImageRepository _imageRepository;
		private IMemoryCache _memoryCache;
		private readonly ILogger<ProductController> _logger;
		private IPaymentHub _paymentHub;
		private IImageService _imageService;
		public ProductController(ILogger<ProductController> logger, IProductRepository productRepository, IImageRepository imageRepository, IMemoryCache memoryCache, IStoreRepository storeRepository, IPaymentHub paymentHub, IImageService imageService)
		{
			_logger = logger;
			_productRepository = productRepository;
			_imageRepository = imageRepository;
			_memoryCache = memoryCache;
			_storeRepository = storeRepository;
			_paymentHub = paymentHub;
			_imageService = imageService;
		}


		[HttpGet("/products")]
		public ActionResult GetAll()
		{
			var products = _productRepository.Get();
			return new JsonResult(products);
		}

		[HttpGet("/product/{id}")]
		public ActionResult Get(string id)
		{
			var allProducts = CacheExtensions.Get<List<Product>>(_memoryCache, "ALL_PRODUCTS");
			var t = allProducts.FirstOrDefault(x => x.id == id);
			var product = _productRepository.GetById(id);
			return new JsonResult(product);
		}

	
		[HttpPost("/product/create")]
		public ActionResult<User> Create([FromBody] ProductRequestModel request)
		{
			HttpContext.Session.SetString("", "Nam Wam");

			request.id = null;
			if (!request.isNew) { return Update(request); }
			var newProduct = new Product(request);
			var response = _productRepository.AddProduct(newProduct);
			return new JsonResult(response);
		}

		[HttpPost("/product/upload")]
		public ActionResult<User> Upload([FromBody] UploadProductRequestModel request)
		{
			var newProduct = new Product() {
				images = new List<ProductImage>(),
				barcode = request.sku
			};

			var imageResponse = _imageService.Upload(new ImageRequestModel() { data = request.image, refId = request.sku });
			newProduct.images.Add(new ProductImage() { publicUrl = imageResponse.SecureUri.ToString(), id = imageResponse.FullyQualifiedPublicId });
			var response = _productRepository.AddProduct(newProduct);
			return new JsonResult(response);
		}

		

		[HttpPost("/product/update")]
		public ActionResult<User> Update([FromBody] ProductRequestModel request)
		{
			var newProduct = new Product(request);
			var response = _productRepository.UpdateProduct(newProduct);
			return new JsonResult(response);
		}

		//IMAGES

		[HttpPost("/product/uploadImage")]
		public ActionResult<User> UploadImage([FromBody] ImageRequestModel request)
		{
			request.type = ImageType.Product;
			if (!request.isNew) { return UpdateImage(request); }
			var product = _productRepository.GetById(request.refId);
			//var response = _imageRepository.AddImage(request);
			var response = new Image() { url = request.url, id=request.id??Guid.NewGuid().ToString()};

			if (product.images == null) product.images = new List<ProductImage>();
			product.images.Add(new ProductImage(response));
			_productRepository.UpdateProduct(product);
			return new JsonResult(response);
		}

		[HttpPost("/product/updateimage")]
		public ActionResult<User> UpdateImage([FromBody] ImageRequestModel request)
		{
			if (request.isNew) { return UploadImage(request); }
			var response = _imageRepository.UpdateImage(request);
			return new JsonResult(response);
		}

		//SEARCH

		[HttpGet("/product/tags/{searchTerm}")]
		public ActionResult GetTags(string searchTerm="")
		{
			var result = new List<string>();
			var allTags = CacheExtensions.Get<IEnumerable<string>>(_memoryCache, "ALL_TAGS");
			result = allTags.ToList();
			if(!string.IsNullOrEmpty(searchTerm))
			{
				result = result.Where(x => x.Contains(searchTerm.ToLower())).ToList();
			}
			return new JsonResult(result);
		}

		[HttpGet("/product/store/{id}/{page}/{searchTerm}/{category}/{size}/{sortOrder}")]
		public ActionResult GetProductsForStore(string id, int page = 0, string searchTerm = "", ProductCategory category = ProductCategory.All, ProductSize size = ProductSize.All, SortTypeEnum sortOrder = SortTypeEnum.ALPH_ASC)
		{
			var allProducts = CacheExtensions.Get<List<Product>>(_memoryCache, "ALL_PRODUCTS");
			var store = _storeRepository.GetById(id);

			var allStoreProducts = new List<StoreProduct>();
			var response = new List<ProductResponseModel>();

			store.aisles.ForEach(x => allStoreProducts.AddRange(x.products.AsEnumerable()));
			allStoreProducts.ForEach(x => response.Add(new ProductResponseModel(x,_productRepository)));

			//Filter
			if (!string.IsNullOrEmpty(searchTerm))
			{
				response = response.Where(x => x.title.ToLower().Contains(searchTerm.ToLower()) ||
				x.description.ToLower().Contains(searchTerm.ToLower())).ToList();
			}
			if (category != ProductCategory.All){response = response.Where(x => x.category == category).ToList();}
			if (size != ProductSize.All){response = response.Where(x => x.size == size).ToList();}

			response = Page(response, page, 5);
			response.OrderByDescending(x => x.title);

			return new JsonResult(response);
		}

		[HttpGet("/product/search/{searchTerm}")]
		public ActionResult Search(string searchTerm, int page = 1)
		{

			var products = _productRepository.Search(searchTerm).ToList();
			var result = Page(products, page, 2);
			return new JsonResult(result);
		}

		//CACHE

		[HttpGet("/product/refresh")]
		public ActionResult RefreshProductCache()
		
		{
			var result = new RefreshProductResponseModel();
			var products = _productRepository.Get().ToList();

			var allTags = new List<string>();
			var tags = products.Select(x => x.tags);
			foreach (var tag in tags)
			{
				var prodTags = tag.Split(",").ToList();
				foreach (var _tag in prodTags)
				{
					if (!allTags.Contains(_tag.ToLower().Trim()))
					{
						allTags.Add(_tag.ToLower().Trim());
					}
				}
			}
			CacheExtensions.Set(_memoryCache, "ALL_PRODUCTS", products);
			CacheExtensions.Set(_memoryCache, "ALL_TAGS", allTags);

			result.products = products;
			_paymentHub.BroadcastResult("hiii","faile");
			//result.tags = allTags;
			//PaymentHub hub = new PaymentHub();
			//hub.TrackPaymentResult(new PaymentTrackRequest());
			return new JsonResult(result);
			//SessionExtensions.Set(Session, "ALL_PRODUCTS", products);
		}

	

	}
}
