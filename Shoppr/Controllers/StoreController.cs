﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using Shoppr.Repositories;
using Shoppr.Models;
using System.Runtime.InteropServices.ComTypes;
using Shoppr.Models.Entities;
using Shoppr.Services;

namespace Shoppr.Controllers
{
	[ApiController]
	[Route("/store")]
	public class StoreController : ControllerBase
	{
		private readonly IOptions<DatabaseSettings> _config;
		private IStoreRepository _storeRepository;
		private IGeoService _geoService;
		private IImageRepository _imageRepository;
		private IProductRepository _productRepository;


		private readonly ILogger<StoreController> _logger;

		public StoreController(ILogger<StoreController> logger, IStoreRepository storeRepository, IProductRepository productRepository, IGeoService geoService, IImageRepository imageRepository)
		{
			_logger = logger;
			_storeRepository = storeRepository;
			_geoService = geoService;
			_imageRepository = imageRepository;
			_productRepository = productRepository;
		}

		[HttpGet("/stores/")]
		public ActionResult Get()
		{
			var stores = _storeRepository.Get();
			return new JsonResult(stores);
		}


		[HttpGet("/store/{id}")]
		public ActionResult Get(string id)
		{
			var store = _storeRepository.GetById(id);
			var response = new StoreResponseModel(store,_productRepository);
			return new JsonResult(response);
		}

		[HttpPost("/store/proximity")]
		public ActionResult<User> GetByProximity([FromBody] StoreProximityRequestModel request)
		{
			var stores = _storeRepository.Get();
			var storesInProvince = stores.Where(x => x.province == request.province);
			var result = new List<Store>();
			foreach(var store in storesInProvince)
			{

				var distance = _geoService.CalculateDistance(new Location() { Latitude = double.Parse(request.latitude.Replace(".",",")), Longitude = double.Parse(request.longitude.Replace(".", ",")) }, 
					new Location() {Latitude = double.Parse(store.latitude.Replace(".", ",")), Longitude = double.Parse(store.longitude.Replace(".", ",")) });
					if (distance < 17000) { result.Add(store); }
			}			
			return new JsonResult(result);
		}


		[HttpPost("/store/create")]
		public ActionResult<User> Create([FromBody] StoreRequestModel request)
		{
			request.id = null;
			var newStore = new Store(request);
			var response = _storeRepository.AddStore(newStore);
			return new JsonResult(response);
		}

		[HttpPost("/store/update")]
		public ActionResult<User> Update([FromBody] StoreRequestModel request)
		{
			var newStore = new Store(request);
			var response = _storeRepository.UpdateStore(newStore);
			return new JsonResult(response);
		}


		[HttpPost("/store/addProductToAisle")]
		public ActionResult<User> AddProductToAisle([FromBody] AddProductToAisleRequest request)
		{
			var store = _storeRepository.GetById(request.storeId);
			if (store == null) return BadRequest();
			var aisle = store.aisles.FirstOrDefault(x => x.aisleId == request.aisleId);
			if (aisle == null)
			{
				aisle = new Aisle() { aisleId = request.aisleId, products = new List<StoreProduct>(), title = request.aisleId };
				aisle.products.Add(request.product);
				store.aisles.Add(aisle);
			}
			else
			{
				aisle.products.Add(request.product);
			}
			
			var response = _storeRepository.UpdateStore(store);
			return new JsonResult(response);
		}

		[HttpPost("/store/addimage")]
		public ActionResult<User> AddImage([FromBody] ImageRequestModel request)
		{
			var store = _storeRepository.GetById(request.refId);
			request.type = ImageType.Store;
			var image = _imageRepository.AddImage(request);
			store.storeImage = image.url;
			var response = _storeRepository.UpdateStore(store);
			return new JsonResult(response);
		}

		[HttpPost("/store/updateimage")]
		public ActionResult<User> UpdateImage([FromBody] ImageRequestModel request)
		{
			var image = _imageRepository.UpdateImage(request);
			return new JsonResult(image);
		}

	}
}
