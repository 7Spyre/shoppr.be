﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Shoppr.Models;
using Shoppr.Models.Entities;
using Shoppr.Repositories;
using Microsoft.AspNetCore.Http;
using Shoppr.Extensions;
using System.Linq;
using static Shoppr.Models.Entities.Product;
using Microsoft.Extensions.Caching.Memory;
using System.Collections.Generic;
using Shoppr.Models.Enums;
using System;
using Shoppr.Services;
using System.Threading.Tasks;
using Shoppr.Models.ResponseModels;
using System.Threading;

namespace Shoppr.Controllers
{
	[ApiController]
	[Route("/payment")]
	public class PaymentController : BaseController
	{
		private IOzowPaymentService _paymentService;
		private IPaymentHub _paymentHub;
		private readonly ILogger<PaymentController> _logger;

		public PaymentController(ILogger<PaymentController> logger, IOzowPaymentService paymentService, IPaymentHub paymentHub)
		{
			_logger = logger;
			_paymentService = paymentService;
			_paymentHub = paymentHub;
		}

		public enum PaymentStatus
		{
			Incomplete =0,
			Succesful =1, 
			Failed =2
		}


		[HttpGet("/trackpayment/{id}")]
		public async Task<ActionResult> GetStatus(string transactionId)
		{
			var result = await _paymentService.TrackPayment(transactionId);
			return new JsonResult(result);
		}


		[HttpPost("/payment/initialize")]
		public ActionResult<InitializePaymentResponseModel> Initialize([FromBody] InitiatePaymentRequestModel request)
		{
			var result = _paymentService.InitializePayment(request);
			return new JsonResult(result);
		}

		[HttpGet("/payment/test")]
		public ActionResult<InitializePaymentResponseModel> Test()
		{
			var result = _paymentService.InitializePayment(new InitiatePaymentRequestModel());
			return new JsonResult(result);
		}


		[HttpPost("/payment/qwsuccess")]
		public ActionResult QwSuccess(OzowPaymentResponseModel response)
		{
			var retryCount = 0;
			var maxRertyCount = 5;
			TrackPaymentResponseModel result = new TrackPaymentResponseModel();

			result = GetStatus(response);
			TrackPaymentResponseModel GetStatus(OzowPaymentResponseModel responseModel)
            {
				var ozowResponse = _paymentService.TrackPayment(response.TransactionId).Result;
				if (ozowResponse.Status != "Success"  && ozowResponse.Status !="Error" && retryCount < maxRertyCount)
				{
					Thread.Sleep(1000);
					retryCount++;
					result = GetStatus(responseModel);
				}
				return result;

			}
			
			return new JsonResult(result);

		}

	

		[HttpPost("/payment/qwerror")]
		public ActionResult<InitializePaymentResponseModel> QwError(OzowPaymentResponseModel response)
		{
			var result = _paymentService.TrackPayment(response.TransactionId);

			return new JsonResult(result);
		}

	}
}
