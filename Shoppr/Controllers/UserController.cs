﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using Shoppr.Repositories;
using Shoppr.Models;
using System.Runtime.InteropServices.ComTypes;
using Shoppr.Services;
using Microsoft.AspNetCore.Cors;

namespace Shoppr.Controllers
{
	[ApiController]
	[Route("/user")]
	public class UserController : ControllerBase
	{
		private readonly IOptions<DatabaseSettings> _config;
		private IUserRepository _userRepository;
		private IPushNotificationService _pushNotificationService;


		private readonly ILogger<UserController> _logger;

		public UserController(ILogger<UserController> logger, IUserRepository userRepository, IPushNotificationService pushNotificationService)
		{
			_logger = logger;
			_userRepository = userRepository;
			_pushNotificationService = pushNotificationService;
		}

		[HttpGet]
		public ActionResult<User> ValidateEmail(string token, string id)
		{
			var user = _userRepository.GetById(id);
			if (user == null) return BadRequest();
			var valid = token == user.emailVerificationCode;
			if (valid == false) return Unauthorized();
			user.lastLogin = DateTime.Now;
			user.status = UserStatus.Verified;
			var response = _userRepository.UpdateUser(user);
			response.secret = "";
			return new JsonResult(response);
		}

		[HttpGet("/user/sendnotification/{id}")]
		public ActionResult<bool> SendNotification(string id)
		{
			_pushNotificationService.SendDeliveryAlert(id);
			return new JsonResult(true);
		}

		[HttpPost("/user/login")]
		public ActionResult<User> Login([FromBody] LoginRequestModel request)
		{
			try
			{
				var user = _userRepository.GetByEmail(request.id?.ToLower());
				if (user == null) return BadRequest();
				var response = _userRepository.VerifyLogin(user.id, request.secret);
				if (response == null) return Unauthorized();
				user.lastLogin = DateTime.Now;
				_userRepository.UpdateUser(user);
				response.secret = "";
				return new JsonResult(response);
			}catch(Exception e)
			{
				return Unauthorized();
			}
			
		}

		[HttpPost("/user/register")]
		public ActionResult<User> Register([FromBody] RegisterRequestModel request)
		{
			var newUser = new User(request);
			var response = _userRepository.AddUser(newUser);
			response.secret = "";
			return new JsonResult(response);
		}

		[HttpPost]
		public ActionResult<User> Update([FromBody] RegisterRequestModel request)
		{
			var newUser = new User(request);
			var response = _userRepository.UpdateUser(newUser);
			response.secret = "";
			return new JsonResult(response);
		}

		//Push notifications

		[HttpPost("/user/push/subscribe")]
		public ActionResult<User> SubscribeToPush([FromBody] SubscribePushNotificationRequestModel requestModel)
		{
			var user = _userRepository.GetById(requestModel.id);
			if (user == null) return BadRequest();
			user.pushNotificationToken = requestModel.token;
			_userRepository.UpdateUser(user);
			return new JsonResult($"Successfully registered user:{requestModel.id}");
		}


	}
}
