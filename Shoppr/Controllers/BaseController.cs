﻿using Microsoft.AspNetCore.Mvc;

using Newtonsoft.Json;
using Shoppr.Models;
using System;
using System.Collections.Generic;
using Shoppr.Models.Interfaces;
using Shoppr.Models.Entities;
using Microsoft.AspNetCore.Http;

namespace Shoppr.Controllers
{
    public class BaseController : ControllerBase
    {
        public bool isValidRequest { get { return GetToken()?.createdDate > DateTime.Now; } }
        public TokenRequest Token { get { return GetToken(); } }
        private TokenRequest GetToken()
        {
          
            var token = Request.Headers["qwikshop-user-token"];
            var result = JsonConvert.DeserializeObject<TokenRequest>(token);
            return result;   
        }

        public ISession Session { get { return HttpContext.Session; } }


        public List<T> Page<T>(List<T> input, int page, int itemsPerPage) where T : Product
        {
            var output = new List<T>();
            var startIndex = page==1|| page==0?1:(page-1) * itemsPerPage;
            var endIndex = startIndex + itemsPerPage;
            var inbounds = input.Count >= startIndex;
            var isFinal = input.Count <= endIndex;
            if (inbounds)
            {
                if (isFinal)
                {
                    for(var c =startIndex; c<=input.Count-1; c++)
                    {
                        output.Add(input[c]);
                    }
                }
                else
                {
                    for (var c = startIndex-1; c < endIndex; c++)
                    {
                        output.Add(input[c]);
                    }
                }
            }
            return output;
        }
    }
}
