﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Shoppr.Models;
using Shoppr.Models.Entities;
using Shoppr.Repositories;
using System.Linq;
using Microsoft.Extensions.Caching.Memory;
using System.Collections.Generic;

namespace Shoppr.Controllers
{
	[ApiController]
	[Route("/voucher")]
	public class VoucherController : BaseController
	{
		private readonly IOptions<DatabaseSettings> _config;
		private IUserRepository _userRepository;
		private IStoreRepository _storeRepository;
		private IVoucherRepository _voucherRepository;
		private IMemoryCache _memoryCache;
		private readonly ILogger<ProductController> _logger;

		public VoucherController(ILogger<ProductController> logger, IUserRepository userRepository, IStoreRepository storeRepository, IVoucherRepository voucherRepository)
		{
			_logger = logger;
			_storeRepository = storeRepository;
			_userRepository = userRepository;
			_voucherRepository = voucherRepository;
		}

		[HttpGet("/vouchers/refreshCache/")]
		public ActionResult RefreshVouchers()
		{
			var result = new RefreshVoucherResponseModel();
			var vouchers = _voucherRepository.Get().ToList();
			
			CacheExtensions.Set(_memoryCache, "ALL_VOUCHERS", vouchers);
			return new JsonResult(result);
		}



		[HttpGet("/voucher/{merchant}/{amount}")]
		public ActionResult GetVoucher(string merchant, string amount)
		{

			var allVouchers = CacheExtensions.Get<List<Voucher>>(_memoryCache, "ALL_VOUCHERS");
			var allAvailableVouchers =allVouchers.Where(c => c.status == VoucherStatus.Available);
			var merchantVouchers = allVouchers.Where(x => x.merchant == merchant);
			var vouchers = new List<Voucher>();
			var balance = double.Parse(amount);
			List<Voucher> GetRequiredVouchers(){
				while (balance > 0)
				{
					var _mVouchers = merchantVouchers.Where(x => x.amount <= balance).OrderByDescending(x=>x.amount);
					var _voucher = _mVouchers.First();
					vouchers.Add(_voucher);
					allVouchers.Where(c => c.id == _voucher.id).First().status = VoucherStatus.InUse;
					CacheExtensions.Set(_memoryCache, "ALL_VOUCHERS", allVouchers);
					balance = balance - _voucher.amount;
				}
				return vouchers;
			}
			return new JsonResult(vouchers);

		}

		[HttpPost("/voucher/create")]
		public ActionResult<User> Create([FromBody] VoucherRequestModel request)
		{
			_voucherRepository.AddVoucher(request);
			return new JsonResult(request);
		}





	}
}
